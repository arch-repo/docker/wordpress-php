FROM registry.gitlab.com/arch-repo/docker/php73:latest

# Update
RUN pacman --noconfirm -Syu

# Install node, composer, ssh, ruby, and yarn
RUN pacman --noconfirm --needed -S nodejs yarn composer base-devel unzip rsync ruby openssh

# Install compass
RUN gem install compass

# Clean up
RUN rm -f \
      /var/cache/pacman/pkg/* \
      /var/lib/pacman/sync/* \
      /etc/pacman.d/mirrorlist.pacnew
